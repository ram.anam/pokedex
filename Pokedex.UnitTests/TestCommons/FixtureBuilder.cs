﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;

namespace Pokedex.UnitTests.TestCommons
{
    public static class FixtureBuilder
    {
        public static IFixture Build()
        {
            var fixture = new Fixture().Customize(new AutoNSubstituteCustomization());
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            return fixture;
        }
    }
}
