﻿using AutoFixture;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Pokedex.Application;
using Pokedex.Application.Interfaces;
using Pokedex.Models;
using Pokedex.UnitTests.TestCommons;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Pokedex.UnitTests.Application
{
    public class PokemonInformationServiceTests
    {
        private readonly IPokemonDataProvider _pokemonDataProvider;
        private readonly PokemonInformationService _sut;
        private readonly IFixture _fixture;
        private readonly ILogger<PokemonInformationService> _logger;
        private readonly IPokemonDescriptionTranslator _translator1, _translator2;

        public PokemonInformationServiceTests()
        {
            _fixture = FixtureBuilder.Build();
            _logger = _fixture.Freeze<ILogger<PokemonInformationService>>();
            _pokemonDataProvider = _fixture.Freeze<IPokemonDataProvider>();

            _translator1 = _fixture.Create<IPokemonDescriptionTranslator>();
            _translator2 = _fixture.Create<IPokemonDescriptionTranslator>();

            var translators = new List<IPokemonDescriptionTranslator>
            { _translator1, _translator2 };
            _fixture.Inject<IReadOnlyList<IPokemonDescriptionTranslator>>(translators);
            
            _sut = _fixture.Create<PokemonInformationService>();
        }

        [Fact]
        public async Task Given_ValidName_Returns_PokenmonInfoReturnedByProvider()
        {
            // Arrange.
            var name = _fixture.Create<string>();
            var pokemonInfo = _fixture.Create<Pokemon>();

            _pokemonDataProvider
                .GetByName(name)
                .Returns(Task.Run(() => pokemonInfo));

            // Act.
            var response = await _sut.GetByName(name);

            // Assert.
            response.Should().BeEquivalentTo(pokemonInfo);
        }

        [Fact]
        public async Task Given_ValidTranslatorOutput_Returns_TranslatedPokemon()
        {
            // Arrange.
            var pokemon = _fixture.Create<Pokemon>();
            var translatedPokemon = _fixture.Create<Pokemon>();

            _translator1
                .CanTranslate(pokemon)
                .Returns(false);

            _translator2
                .CanTranslate(pokemon)
                .Returns(true);
            
            _translator2
                .Translate(pokemon)
                .Returns(Task.Run(() => translatedPokemon));

            _pokemonDataProvider
                .GetByName(pokemon.Name)
                .Returns(Task.Run(() => pokemon));

            // Act.
            var response = await _sut.GetTranslatedByName(pokemon.Name);

            // Assert.
            response
                .Should()
                .BeEquivalentTo(translatedPokemon);
        }

        [Fact]
        public async Task Given_TranslatorThrowsException_Returns_PokemonInformation()
        {
            // Arrange.
            var pokemon = _fixture.Create<Pokemon>();
            var translatedPokemon = _fixture.Create<Pokemon>();

            _translator1
                .CanTranslate(pokemon)
                .Returns(false);

            _translator2
                .CanTranslate(pokemon)
                .Returns(true);

            _translator2
                .Translate(pokemon)
                .Throws(new System.Exception());

            _pokemonDataProvider
                .GetByName(pokemon.Name)
                .Returns(Task.Run(() => pokemon));

            // Act.
            var response = await _sut.GetTranslatedByName(pokemon.Name);

            // Assert.
            response
                .Should()
                .BeEquivalentTo(pokemon);
        }
    }
}
