﻿using AutoFixture;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Pokedex.Infrastructure.PokeApi;
using Pokedex.Infrastructure.PokeApi.Dto;
using Pokedex.Shared;
using Pokedex.UnitTests.TestCommons;
using Refit;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Pokedex.UnitTests.Infrastructure.PokeApi
{
    public class PokeApiDataProviderTests
    {
        private readonly IPokeApiProxy _pokeApiProxy;
        private readonly PokeApiDataProvider _sut;
        private readonly ILogger<PokeApiDataProvider> _logger;
        private IFixture _fixutre;

        public PokeApiDataProviderTests()
        {
            _fixutre = FixtureBuilder.Build();
            _pokeApiProxy = _fixutre.Freeze<IPokeApiProxy>();
            _logger = _fixutre.Freeze<ILogger<PokeApiDataProvider>>();
            _sut = _fixutre.Create<PokeApiDataProvider>();
        }

        [Fact]
        public async Task Given_ValidName_Should_Return_PokemonInformation()
        {
            // Arrange.
            var name = _fixutre.Create<string>();

            var enLanguage = new Language { Name = "en" };
            var dkLanguage = new Language { Name = "dk" };

            var pokemonSpeciesResponse = new PokemonSpecies
            {
                Id = 1,
                Name = name,
                IsLegendry = false,
                Habitat = new Habitat
                {
                    Id = 5,
                    Name = "rare"
                },
                FlavorTextEntries = new List<FlavorText>
                 {
                     new FlavorText
                     {
                         FlavourText = "EN Flavour Text",
                         Language = enLanguage
                     },
                     new FlavorText
                     {
                         FlavourText = "DK Flavour Text",
                         Language =  dkLanguage
                     }
                 }
            };

            _pokeApiProxy
                .GetByName(Arg.Is(name))
                .Returns(Task.Run(() => pokemonSpeciesResponse));

            // Act.
            var response = await _sut.GetByName(name);

            // Assert.
            var expectedPokemon = new Models.Pokemon(name, "EN Flavour Text", "rare", false);
            response.Should().BeEquivalentTo(expectedPokemon);
        }

        [Fact]
        public async Task Given_PokeApi_DoesNotHas_FlavourTextInEN_Returns_Null_Description()
        {
            // Arrange.
            var name = _fixutre.Create<string>();

            var enLanguage = new Language { Name = "en" };
            var dkLanguage = new Language { Name = "dk" };

            var pokemonSpeciesResponse = new PokemonSpecies
            {
                Id = 1,
                Name = name,
                IsLegendry = false,
                Habitat = new Habitat
                {
                    Id = 5,
                    Name = "rare"
                },
                FlavorTextEntries = new List<FlavorText>
                 {
                     new FlavorText
                     {
                         FlavourText = "DK Flavour Text",
                         Language =  dkLanguage
                     }
                 }
            };

            _pokeApiProxy
                .GetByName(Arg.Is(name))
                .Returns(Task.Run(() => pokemonSpeciesResponse));

            // Act.
            var response = await _sut.GetByName(name);

            // Assert.
            var expectedPokemon = new Models.Pokemon(name, null, "rare", false);
            response.Should().BeEquivalentTo(expectedPokemon);
        }

        [Fact]
        public async Task Given_PokeApiReturnsNotFound_Should_Throw_Exception()
        {
            // Arrange.
            var name = _fixutre.Create<string>();

            var refitSettings = new RefitSettings();
            var notFoundResult = await ApiException.Create(null,
                HttpMethod.Get,
                new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.NotFound
                },
                refitSettings);

            _pokeApiProxy
                .GetByName(Arg.Is(name))
                .Throws(notFoundResult);

            // Act.
            var exception = await Assert.ThrowsAsync<PokedexApiException>(() => _sut.GetByName(name));

            // Assert.
            exception.HttpStatusCode.Should().Be(HttpStatusCode.NotFound);
            exception.ErrorCode.Should().Be("404.001");
            exception.Message.Should().Be($"Pokemon info {name} not found");
        }
 
        [Fact]
        public async Task Given_PokeApiReturnsError_Should_Throw_Exception()
        {
            // Arrange.
            var name = _fixutre.Create<string>();

            var refitSettings = new RefitSettings();
            var notFoundResult = await ApiException.Create(null,
                HttpMethod.Get,
                new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.TooManyRequests
                },
                refitSettings);

            _pokeApiProxy
                .GetByName(Arg.Is(name))
                .Throws(notFoundResult);

            // Act.
            var exception = await Assert.ThrowsAsync<PokedexApiException>(() => _sut.GetByName(name));

            // Assert.
            exception.HttpStatusCode.Should().Be(HttpStatusCode.InternalServerError);
            exception.ErrorCode.Should().Be("500.001");
            exception.Message.Should().Be($"Error occured while retrieving information for {name}");
        }
    }
}
