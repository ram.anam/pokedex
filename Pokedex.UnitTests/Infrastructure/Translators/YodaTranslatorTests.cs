﻿using AutoFixture;
using FluentAssertions;
using NSubstitute;
using Pokedex.Infrastructure.Translators;
using Pokedex.Infrastructure.Translators.Dto;
using Pokedex.Models;
using Pokedex.UnitTests.TestCommons;
using System.Threading.Tasks;
using Xunit;

namespace Pokedex.UnitTests.Infrastructure.Translators
{
    public class YodaTranslatorTests
    {
        private readonly YodaTranslator _sut;
        private readonly IFunTranslationApiProxy _translationApiProxy;
        private readonly IFixture _fixture;

        public YodaTranslatorTests()
        {
            _fixture = FixtureBuilder.Build();
            _translationApiProxy = _fixture.Freeze<IFunTranslationApiProxy>();
            _sut = _fixture.Create<YodaTranslator>();
        }

        [Theory]
        [InlineData("cave", true)]
        [InlineData("caVe", true)]
        [InlineData("Forest", false)]
        public void Given_PokemonWithHabitat_CanTranslate_ReturnsExpected(string habitat, bool expected)
        {
            // Arrange.
            var pokemon = new Pokemon(
                _fixture.Create<string>(),
                _fixture.Create<string>(),
                habitat,
                false);

            // Act.
            var canTranslate = _sut.CanTranslate(pokemon);

            // Assert
            canTranslate
                .Should()
                .Be(expected);
        }

        [Fact]
        public void Given_LegendryPokemon_CanTranslate_ReturnsTrue()
        {
            // Arrange.
            var pokemon = new Pokemon(
                _fixture.Create<string>(),
                _fixture.Create<string>(),
                null,
                true);

            // Act.
            var canTranslate = _sut.CanTranslate(pokemon);

            // Assert
            canTranslate
                .Should()
                .BeTrue();
        }
    
        [Fact]
        public async Task Given_ValidPokemon_Translate_ReturnsTranslated()
        {
            // Arrange.
            var pokemon = _fixture.Create<Pokemon>();
            var translateResponse = _fixture.Create<TranslateResponse>();

            _translationApiProxy.
                YodaTranslate(Arg.Is<TranslateRequest>(r => r.Text == pokemon.Description)).
                Returns(Task.Run(() => translateResponse));

            // Act.
            var translated = await _sut.Translate(pokemon);

            // Assert
            var expected = new Pokemon(
                pokemon.Name,
                translateResponse.Contents.Translated,
                pokemon.Habitat,
                pokemon.IsLegendry);

            translated
                .Should()
                .BeEquivalentTo(expected);
        }
    }
}
