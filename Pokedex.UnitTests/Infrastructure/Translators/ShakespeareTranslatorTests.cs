﻿using AutoFixture;
using FluentAssertions;
using NSubstitute;
using Pokedex.Infrastructure.Translators;
using Pokedex.Infrastructure.Translators.Dto;
using Pokedex.Models;
using Pokedex.UnitTests.TestCommons;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pokedex.UnitTests.Infrastructure.Translators
{
    public class ShakespeareTranslatorTests
    {
        private readonly ShakespeareTranslator _sut;
        private readonly IFunTranslationApiProxy _translationApiProxy;
        private readonly IFixture _fixture;

        public ShakespeareTranslatorTests()
        {
            _fixture = FixtureBuilder.Build();
            _translationApiProxy = _fixture.Freeze<IFunTranslationApiProxy>();
            _sut = _fixture.Create<ShakespeareTranslator>();
        }

        [Fact]
        public void Given_Pokemn_CanTranslate_ReturnsTrue()
        {
            // Arrange.
            var pokemon = _fixture.Create<Pokemon>();

            // Act.
            var result = _sut.CanTranslate(pokemon);

            // Assert.
            result
                .Should()
                .BeTrue();
        }

        [Fact]
        public async Task Given_ValidPokemon_Translate_ReturnsTranslated()
        {
            // Arrange.
            var pokemon = _fixture.Create<Pokemon>();
            var translateResponse = _fixture.Create<TranslateResponse>();

            _translationApiProxy.
                ShakespeareTranslate(Arg.Is<TranslateRequest>(r => r.Text == pokemon.Description)).
                Returns(Task.Run(() => translateResponse));

            // Act.
            var translated = await _sut.Translate(pokemon);

            // Assert
            var expected = new Pokemon(
                pokemon.Name,
                translateResponse.Contents.Translated,
                pokemon.Habitat,
                pokemon.IsLegendry);

            translated
                .Should()
                .BeEquivalentTo(expected);
        }
    }
}
