﻿using Microsoft.Extensions.Logging;
using Pokedex.Application.Interfaces;
using Pokedex.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Application
{
    public interface IPokemonInformationService
    {
        Task<Pokemon> GetByName(string name);
        Task<Pokemon> GetTranslatedByName(string name);
    }

    public class PokemonInformationService : IPokemonInformationService
    {
        private readonly IPokemonDataProvider _dataProvider;
        private readonly IReadOnlyList<IPokemonDescriptionTranslator> _descriptionTranslators;
        private readonly ILogger<PokemonInformationService> _logger;

        public PokemonInformationService(
            IPokemonDataProvider pokemonDataProvider, 
            IReadOnlyList<IPokemonDescriptionTranslator> descriptionTranslators, 
            ILogger<PokemonInformationService> logger)
        {
            _dataProvider = pokemonDataProvider;
            _descriptionTranslators = descriptionTranslators;
            _logger = logger;
        }

        public Task<Pokemon> GetByName(string name) => _dataProvider.GetByName(name);

        public async Task<Pokemon> GetTranslatedByName(string name)
        {
            var pokemon = await _dataProvider.GetByName(name);

            var matchingTranslator = _descriptionTranslators
                .Where(t => t.CanTranslate(pokemon))
                .OrderBy(t => t.Order)
                .First();

            try
            {
                var translatedPokemon = await matchingTranslator.Translate(pokemon);
                return translatedPokemon;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error translating Pokemon using {matchingTranslator.Name}", ex);
                return pokemon;
            }
        }
    }
}
