﻿using Pokedex.Models;
using System.Threading.Tasks;

namespace Pokedex.Application.Interfaces
{
    public interface IPokemonDataProvider
    {
        public Task<Pokemon> GetByName(string name);
    }
}
