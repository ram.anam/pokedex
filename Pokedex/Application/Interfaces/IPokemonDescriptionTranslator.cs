﻿using Pokedex.Models;
using System.Threading.Tasks;

namespace Pokedex.Application.Interfaces
{
    public interface IPokemonDescriptionTranslator
    {
        string Name { get; }
        int Order { get; }
        bool CanTranslate(Pokemon pokemon);
        Task<Pokemon> Translate(Pokemon pokemon);
    }
}
