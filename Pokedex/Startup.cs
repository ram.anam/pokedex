using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Pokedex.Application;
using Pokedex.Application.Interfaces;
using Pokedex.Infrastructure.PokeApi;
using Pokedex.Infrastructure.Translators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pokedex
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // Configure Application
            services
                .AddTransient<IPokemonInformationService, PokemonInformationService>()
                .AddTransient<IPokemonDataProvider, PokeApiDataProvider>();

            // Configure Infrastructure.
            services
                .AddHttpClient("PokeApi",
                               c =>
                               {
                                   c.BaseAddress = new Uri("https://pokeapi.co/api/v2");
                               })
                .AddTypedClient(c => Refit.RestService.For<IPokeApiProxy>(c));

            services
                .AddHttpClient("FunTranslations",
                               c =>
                               {
                                   c.BaseAddress = new Uri("https://api.funtranslations.com/translate");
                               })
                .AddTypedClient(c => Refit.RestService.For<IFunTranslationApiProxy>(c));

            services
                .AddTransient<IPokemonDescriptionTranslator, YodaTranslator>()
                .AddTransient<IPokemonDescriptionTranslator, ShakespeareTranslator>()
                .AddTransient<IReadOnlyList<IPokemonDescriptionTranslator>>(sp => 
                {
                    var allTranslators = sp.GetServices<IPokemonDescriptionTranslator>();
                    return allTranslators.ToList();
                });

            // Swagger
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pokedex", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "swagger/ui";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pokedex API");
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
