﻿using Pokedex.Application.Interfaces;
using Pokedex.Infrastructure.Translators.Dto;
using Pokedex.Models;
using System.Threading.Tasks;

namespace Pokedex.Infrastructure.Translators
{
    public class ShakespeareTranslator : IPokemonDescriptionTranslator
    {
        private readonly IFunTranslationApiProxy _translateApiProxy;

        public ShakespeareTranslator(IFunTranslationApiProxy translateApiProxy)
        {
            _translateApiProxy = translateApiProxy;
        }

        public string Name => "Shakespeare";

        public int Order => 2;

        public bool CanTranslate(Pokemon pokemon) => true;

        public async Task<Pokemon> Translate(Pokemon pokemon)
        {
            var translateRequest = new TranslateRequest
            {
                Text = pokemon.Description
            };

            var translatedResponse = await _translateApiProxy.ShakespeareTranslate(translateRequest);

            return new Pokemon(
                pokemon.Name,
                translatedResponse.Contents.Translated,
                pokemon.Habitat,
                pokemon.IsLegendry);
        }
    }
}
