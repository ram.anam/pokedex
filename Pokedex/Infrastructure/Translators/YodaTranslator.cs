﻿using Pokedex.Application.Interfaces;
using Pokedex.Infrastructure.Translators.Dto;
using Pokedex.Models;
using System.Threading.Tasks;

namespace Pokedex.Infrastructure.Translators
{
    public class YodaTranslator : IPokemonDescriptionTranslator
    {
        private readonly IFunTranslationApiProxy _translateApiProxy;

        public YodaTranslator(IFunTranslationApiProxy translateApiProxy)
        {
            _translateApiProxy = translateApiProxy;
        }

        public string Name => "Yoda";

        public int Order => 1;

        public bool CanTranslate(Pokemon pokemon)
        => string.Compare(pokemon.Habitat, "cave", true) == 0 ||
           pokemon.IsLegendry;

        public async Task<Pokemon> Translate(Pokemon pokemon)
        {
            var translateRequest = new TranslateRequest
            {
                Text = pokemon.Description
            };

            var translatedResponse = await _translateApiProxy.YodaTranslate(translateRequest);

            return new Pokemon(
                pokemon.Name,
                translatedResponse.Contents.Translated,
                pokemon.Habitat,
                pokemon.IsLegendry);
        }
    }
}
