﻿namespace Pokedex.Infrastructure.Translators.Dto
{
    public class SuccessCount
    {
        public int Total { get; set; }
    }
}
