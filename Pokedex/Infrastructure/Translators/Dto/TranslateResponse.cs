﻿namespace Pokedex.Infrastructure.Translators.Dto
{
    public class TranslateResponse
    {
        public SuccessCount Success { get; set; }
        public TranslatedContents Contents { get; set; }
    }
}
