﻿namespace Pokedex.Infrastructure.Translators.Dto
{
    public class TranslateRequest
    {
        public string Text { get; set; }
    }
}
