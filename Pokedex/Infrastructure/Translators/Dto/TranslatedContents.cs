﻿namespace Pokedex.Infrastructure.Translators.Dto
{
    public class TranslatedContents
    {
        public string Translated { get; set; }
        public string Text { get; set; }
        public string Translation { get; set; }
    }
}
