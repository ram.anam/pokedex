﻿using Pokedex.Infrastructure.Translators.Dto;
using Refit;
using System.Threading.Tasks;

namespace Pokedex.Infrastructure.Translators
{
    public interface IFunTranslationApiProxy
    {
        [Post("/yoda.json")]
        public Task<TranslateResponse> YodaTranslate(TranslateRequest request);

        [Post("/shakespeare.json")]
        public Task<TranslateResponse> ShakespeareTranslate(TranslateRequest request);
    }
}
