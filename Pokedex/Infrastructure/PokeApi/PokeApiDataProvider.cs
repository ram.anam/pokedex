﻿using Microsoft.Extensions.Logging;
using Pokedex.Application.Interfaces;
using Pokedex.Infrastructure.PokeApi.Dto;
using Pokedex.Models;
using Pokedex.Shared;
using Refit;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Pokedex.Infrastructure.PokeApi
{
    public class PokeApiDataProvider : IPokemonDataProvider
    {
        private const string EnglishCode = "en";
        private readonly IPokeApiProxy _pokeApiProxy;
        private readonly ILogger<PokeApiDataProvider> _logger;

        public PokeApiDataProvider(IPokeApiProxy pokeApiProxy, ILogger<PokeApiDataProvider> logger)
        {
            _pokeApiProxy = pokeApiProxy;
            _logger = logger;
        }

        public async Task<Pokemon> GetByName(string name)
        {
            try
            {
                var pokemonSpecies = await _pokeApiProxy.GetByName(name);
                return Map(pokemonSpecies);
            }
            catch (ApiException ex)
            {
                _logger.LogError($"Error Getting info for {name}", ex);

                switch (ex.StatusCode)
                {
                    case HttpStatusCode.NotFound: 
                            throw new PokedexApiException(HttpStatusCode.NotFound, "404.001", $"Pokemon info {name} not found");
                    default:
                        throw new PokedexApiException(HttpStatusCode.InternalServerError, "500.001", $"Error occured while retrieving information for {name}");
                }
            }
        }

        private static Pokemon Map(PokemonSpecies pokemonSpecies)
        {
            var description = pokemonSpecies
                .FlavorTextEntries?
                .FirstOrDefault(fd => fd.Language.Name == EnglishCode)?
                .FlavourText;

            return new Pokemon(pokemonSpecies.Name, description, pokemonSpecies.Habitat?.Name, pokemonSpecies.IsLegendry);
        }
    }
}
