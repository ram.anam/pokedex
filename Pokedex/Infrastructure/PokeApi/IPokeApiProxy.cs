﻿using Pokedex.Infrastructure.PokeApi.Dto;
using Refit;
using System.Threading.Tasks;

namespace Pokedex.Infrastructure.PokeApi
{
    public interface IPokeApiProxy
    {
        [Get("/pokemon-species/{pokemonName}")]
        public Task<PokemonSpecies> GetByName(string pokemonName);
    }
}
