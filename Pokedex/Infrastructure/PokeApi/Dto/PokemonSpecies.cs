﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Pokedex.Infrastructure.PokeApi.Dto
{
    public class PokemonSpecies
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonPropertyName("flavor_text_entries")]
        public List<FlavorText> FlavorTextEntries { get; set; }

        [JsonPropertyName("is_legendry")]
        public bool IsLegendry { get; set; }
        public Habitat Habitat { get; set; }
    }
}
