﻿using System.Text.Json.Serialization;

namespace Pokedex.Infrastructure.PokeApi.Dto
{
    public class FlavorText
    {
        [JsonPropertyName("flavor_text")]
        public string FlavourText { get; set; }
        public Language Language { get; set; }
    }
}
