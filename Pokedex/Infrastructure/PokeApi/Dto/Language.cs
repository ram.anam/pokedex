﻿namespace Pokedex.Infrastructure.PokeApi.Dto
{
    public class Language
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
