﻿namespace Pokedex.Infrastructure.PokeApi.Dto
{
    public class Habitat
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
