﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pokedex.Application;
using Pokedex.Models;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace Pokedex.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PokemonController : ControllerBase
    {
        private readonly IPokemonInformationService _pokemonInformationService;

        public PokemonController(IPokemonInformationService pokemonInformationService)
        {
            _pokemonInformationService = pokemonInformationService;
        }

        [HttpGet("{pokemonName}")]
        [ProducesResponseType(type: typeof(Pokemon), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(
            [FromRoute]
            [Required]
            string pokemonName)
        => Ok(await _pokemonInformationService.GetByName(pokemonName));

        [HttpGet("translated/{pokemonName}")]
        [ProducesResponseType(type: typeof(Pokemon), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetTranslated(
            [FromRoute]
            [Required]
            string pokemonName)
        => Ok(await _pokemonInformationService.GetTranslatedByName(pokemonName));
    }
}
