﻿using System;
using System.Net;

namespace Pokedex.Shared
{
    public class PokedexApiException : Exception
    {
        public HttpStatusCode HttpStatusCode { get; private set; }
        public string ErrorCode { get; private set; }

        public PokedexApiException(HttpStatusCode httpStatusCode, string errorCode, string message, object details = null)
            : base(message)
        {
            HttpStatusCode = httpStatusCode;
            ErrorCode = errorCode;
        }
    }
}
