FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Pokedex/Pokedex.csproj", "Pokedex/"]
COPY ["Pokedex.UnitTests/Pokedex.UnitTests.csproj", "Pokedex.UnitTests/"]

RUN dotnet restore Pokedex/Pokedex.csproj
RUN dotnet restore Pokedex.UnitTests/Pokedex.UnitTests.csproj

COPY Pokedex/ Pokedex
COPY Pokedex.UnitTests/ Pokedex.UnitTests



WORKDIR "/src/Pokedex"
RUN dotnet build "Pokedex.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Pokedex.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Pokedex.dll"]