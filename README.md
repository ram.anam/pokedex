
# Pokedex Coding Exercise

## Development Environment & Libraries
- VS 2019
- .Net Core 3.1
- Dev libraries - Refit, Swashbuckle
- Test libraries - XUnit, NSubstiture and Autofixture for tests

## How to Run - Non VS 2019 
- Install .Net Core 3.1 SDK as per the instructions provided at [https://dotnet.microsoft.com/download/dotnet/3.1]
- Clone the repository
- Open command prompt
- Change the directory to the location where code is checked out
- Run following commands in order
	- `dotnet build` This command will build the code 
	- `dotnet test` This command will run the tests
	-  `cd Pokedex`
	- `dotnet run` This command will run the API
- Open web browser and navigate to [http://localhost:5000/swagger/ui/] to see the Swagger UI for the Pokedex API

## How to Run - Docker - Windows
- Install docker desktop
- Clone the repository
- Open command prompt
- Change the directory to the location where code is checked out
- Run `docker build -t pokedex:latest .` to build docker image
- Run `docker container run -d --name pokedex_latest -p 8080:80  pokedex:latest` to run the container built out of image
- Open web browser and navigate to [http://localhost:8080/swagger/ui/index.html] to access the swagger ui.
- Run `docker container stop {conatiner id}` to stop the container

## Improvements for Production readiness 
#### Code
- Right now all code is added to same project Pokedex. But actually each folder will should be a different class library
	- Application - Containing business logic and orchestration
	- Infrastructure - Any third party dependencies required by application
	- Shared - Any shared code like exceptions
- Middleware to catch and log all exceptions
- Middleware to log ApiExceptions/Exceptions and return standard error responses
- Middleware for correlation ids
- Logic to handle Model Validation Errors and return standard responses
- Integrating Logging library like Serilog for logging
- Improve error handling for third party api integrations
- Using cancellation tokens in entire request life cycle
- Improve swagger documentation using xml comments - sample requests, response, error codes
#### Test Coverage
- Integration tests with stubs for third party api
